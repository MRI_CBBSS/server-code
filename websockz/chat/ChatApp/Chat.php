<?php
namespace ChatApp;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

//$dataFile = fopen("dataFile.txt", "w");

class Sensor{
  public $initialized;
  public $userID;
  public $sensorID;
  public $type;
  public $units;
  public $humanST;
  public $streamID;
  public $fileName;
  public $sensorFile;
}

class Chat implements MessageComponentInterface {
  //protected $db;
  protected $clients;
  protected $count;
  protected $timing_count;
  protected $time_start;
  protected $time_end;

  protected $sensors;

  public function dir_is_empty($dir) {
    $h = opendir($dir);
    while (($entry = readdir($h)) !== false) {
      if($entry != "." && $entry != "..") {
        return FALSE;
      }
    }
    return TRUE;
  }

  public function most_recent_entry() {
    $query = "SELECT * FROM Datasets ORDER BY dataId DESC LIMIT 0,1";
    if($this->conn->query($query))
    {
      $res = mysqli_query($this->conn, $query);
      $r = mysqli_fetch_object($res);
      return $r;
    }
    return -1;
  }

  public function writeToCSV($sense, $data) {
    $fName = $sense->fileName;
    $sfName = $sense->sensorFile;
    //echo "masterFileName: ".$sfName."\n";
    $result = shell_exec('python ChatApp/writeCSV1.py ' . $data . ' ' . $fName);
    //the following is under construction in writeCSV.py:
    //if(count($sense->type) > 1)
    //  $result2 = shell_exec('python ChatApp/writeCSV.py ' . $data . ' ' . $sfName . " 1 " . $sense->type[1]);
    //else
    //  $result2 = shell_exec('python ChatApp/writeCSV.py ' . $data . ' ' . $sfName . " 2");
  }

  //checks if current sensor should be using a previous entry's info or to create new.
  //assigns globals
  public function db_entry_check($sense, $client) {
    $t = implode('', $sense->type);
    $query = "SELECT * FROM Datasets
                   WHERE (done = 0)
                   AND (userId = '$sense->userID')
		   AND (type = '$t')
		   AND (source = 'app')";
    $result = mysqli_query($this->conn, $query);
    $r = mysqli_fetch_array($result);
    $id = $r['streamId'];
    if(!($id == ''))
    {
      //echo "found\n";
      $sense->fileName = $r['fileName'];
    }
    else
    {
      $query = "SELECT * FROM Datasets
		     WHERE (done = 0)
		     AND (userId = '$sense->userID')";
      $result = mysqli_query($this->conn, $query);
      $r = mysqli_fetch_array($result);
      $sID = $r['streamId'];
      if($sID == '')
        $sID = $this->most_recent_entry()->streamId + 1;

      $sense->streamID = $sID;
      $id = $sID;
      $typeSize = count($sense->type);
      $fname = "ChatApp/dataFiles/".$sense->sensorID."_".$sense->type[0].$sense->type[$typeSize-1]."_".$sense->streamID.".csv";
      $sense->fileName = $fname;
      $t = implode('', $sense->type);
      $g = $sense->type[0];
      $query = "INSERT INTO Datasets (userId, sensorId, type, typeGen, units, humanSubjectType, source, fileName, streamID) VALUES ('$sense->userID', '$sense->sensorID', '$t', '$g', '$sense->units', '$sense->humanST', 'app', '$fname', '$sID')";
      $result = mysqli_query($this->conn, $query) or die(mysqli_error($this->conn)."\n");
    }
    $sense->sensorFile = "ChatApp/dataFiles/".$sense->sensorID."_".$sense->type[0]."_".$id.".csv";
    $client->send($id);		//send streamID to tablet for later use
    echo "\nMessage has been sent to the tablet\n";
  }

  public function __construct() {
    $this->clients = new \SplObjectStorage;

    $servername = "localhost";
    $usr = "matt";
    $passw = "CSci@5657";

    $this->conn = mysqli_connect($servername, $usr, $passw, "csufml_db");
    if(mysqli_connect_errno())
    {
      die("Connection failed: " . $this->conn->connect_error . "\n");
    }
    //else
      //echo "db Success!\n";

    echo "initiating process\n";

  }

  public function onOpen(ConnectionInterface $conx) {
    // Store the new connection
    $this->clients->attach($conx);
    $this->count = 0;
    $this->timing_count = 0;
    $this->streamID = -1;
    $conx->send("you connected!");
    echo "Someone connected\n";
  }

  public function onMessage(ConnectionInterface $from, $msg) {

    //if this is the first message of the stream
    $s = spl_object_hash($from);

    $exists = false;
    if(empty($this->sensors))
      $exists = false;
    else{
      $exists = array_key_exists("{$s}", $this->sensors);
    }

    $msgArr = explode('~', $msg);
    if(!empty($msgArr))
    {
       //echo $msgArr[0]." asdfreceived!";
    }

    if(!$exists)
    {
      $this->clients->attach($from);

      $msgArr = explode('~', $msg);
      $newSensor = new Sensor();
      $newSensor->userID = $msgArr[0];
      $newSensor->sensorID = $msgArr[1];
      $newSensor->type = explode(' ', $msgArr[2]);
      $newSensor->units = $msgArr[3];
      $newSensor->humanST = $msgArr[4];

      $this->sensors["{$s}"] = $newSensor;

      $this->db_entry_check($newSensor, $from);
    }
    else if($msg == "Do analysis"){
      //trigger analysis, "snapshot" files, combine data
      $sense = $this->sensors["{$s}"];
      //if($sense->type[0] == "Gyroscope"){
	//$query = "INSERT INTO "
      //}
      $res = shell_exec("python ChatApp/combineCSVs.py " . $sense->sensorID . " " . $sense->type[0] . " " . $sense->streamID);
      $res1 = shell_exec("python ChatApp/dummyAlgo1.py " . $sense->sensorID . " " . $sense->type[0] . " " . $sense->streamID);
      $res2 = shell_exec("python ChatApp/dummyAlgo2.py " . $sense->sensorID . " " . $sense->type[0] . " " . $sense->streamID);
      $from->send("average:" . $res1 . " min:" . $res2);
    }
    else
    {
      echo $msg."\n";
      $this->writeToCSV($this->sensors["{$s}"], $msg);
    }
    //echo $msg."\n";
    // Send the message to all the other clients except the one who sent
    //foreach ($this->clients as $client) {
      //if ($from == $client) {
        //$userdb =
      //}
      //else {
        //Send the message to all the other clients except the one who sent
        //$client->send($msg);
      //}
    //}
	//sned a message back to the user and print the message to the terminal
	//$from->send($msg);
/*
	if($msg == "first_in_segment") {
	  $this->time_start = new \DateTime();
	  $this->timing_count = 0;
	}
	if($msg == "last_in_segment") {
	  $this->time_end = new \DateTime();
	  //echo $this->time_start->format('Y-m-d H:i:s');
	  //echo "\n";
	  echo $this->time_end->format('Y-m-d H:i:s');
	  echo "\n";
	  echo $this->timing_count;
	  echo "\n";
	}

	if($msg == "packet_start")
	  $this->timing_count = $this->timing_count + 1;
        //echo $msg;
	//echo " ";
	//if ($this->count % 3 == 0)
	//	echo "\n";
 	//$this->count = $this->count +1;
	//$decodeValue = decodeMsg($msg);
	//fwrite($dataFile, $msg);
	//fwrite($dataFile, "\n");

*/
  }

  public function onClose(ConnectionInterface $conx) {
    $this->clients->detach($conx);
    $s = spl_object_hash($conx);
    $sen = $this->sensors["{$s}"];
    $fileName = "ChatApp/dataOutputs/".$sen->type[0].$sen->streamID."out/";
    if(!file_exists($fileName)){
      $result = shell_exec("python ChatApp/combineCSVs.py " . $sen->sensorID . " " . $sen->type[0] . " " . $sen->streamID);
    }

    //$result = shell_exec("python ChatApp/writeCSV2.py ".$sen->sensorID.' '.$sen->type[0].' '.$sen->streamID);

    echo "Someone left";
  }

  public function onError(ConnectionInterface $conx, \Exception $e) {
    echo "An error occurred: {$e->getMessage()}\n";

    $conx->close();
  }
}
?>

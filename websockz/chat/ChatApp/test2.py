import csv
import itertools as IT
import fnmatch
import os

for file in os.listdir('./dataFiles'):
  if fnmatch.fnmatch(file, '*4.csv') and fnmatch.fnmatch(file, '*Gyroscope*'):
    print file

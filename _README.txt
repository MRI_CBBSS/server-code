/*****************************************************************************************************************************/
Webpages all require Bootstrap and jQuery to function properly.
- If some CSS is not working, perhaps you need to make it "!important" in order to override boostrap's css - or css file has been cached
- Also, some bootstrap classes are used within the html/php files like class='border border-dark' instead of using CSS.

Sass was used to make styling a bit easier, but editing the raw CSS files are good enough.

Database (mysql) configuration file is located in the folder otherFiles

Was unable to fully complete the email portion, but got a working prototype.
Using PHPmailer, the file serverFiles/sendMail.php uses the SMTP to send an email from one email account to another
Used on my own emails but it raised a security concern from google - claimed server has known security problems or is out of date
Should work if the activity is approved.

Main Webpages:
  index.html       // Home page
  reference.html   // Detailed info about algorithms/data/sensors
  login.php        // Login and Registration Page
  account.php      // Account page
  repository.php   // The CBBSS tool used to run algorithms

Folders included:
  datasets   // Not necessary yet
  imgs       // Contains sponsor images for the webpages to use
  lists      // Contains text files with all algorithms, data, and sensors that should be displayed in the repository page.
  otherFiles // Contains dbsetup.py - configuration of database
  testFiles  // Contains statEx.py  - the statsmodel example test
  sassFiles  // Contains sass files for account_css, common_css, form_css, and repository_css - Not necessary to use, but helpful

  styles      // Contains all CSS files required by the webpages
  scripts     // Contains all JavaScript files required by the webpages
  serverFiles // Contains all PHP files that run on the server
    userData  // Contains all users uploaded data - consider moving folder to prevent access to all files via a link

Permission:
  file/dir    - owner:group - permissions [r=4/w=2/e=1] | 7 = rwx, 6 = rw, 5 = rx

  // Important
  serverFiles  - www-data:www-data - 755
    // shows all user data by going to the bsncloud.csufresno.edu/serverFiles/userData
    userData   - www-data:www-data - 755
    // hides the folder, but individual folders within are still reachable via a url link
    userData   - www-data:www-data - 355

    // Required to download and access files
    userData/* - www-data:www-data 755

  // Not so important
  index.html     - root:root - 644
  reference.php  - root:root - 644
  login.php      - root:root - 644
  repository.php - root:root - 644
  account.php    - root:root - 644

  // Any permission - As long as files can be read and executed
  root:root - 755 for all other files/dirs

/*****************************************************************************************************************************/

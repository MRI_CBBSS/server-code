<?php
  include "database.php";

  //Page with tool used for computation of algorithm with given data

  //Check if user has access to the page
  session_start();
  if(!isset($_SESSION['logged_in'])){
    session_unset();
    $_SESSION['logged_in'] = false;
  }
  if(!$_SESSION['logged_in']){
    header('Location: index.html');
    die("Permission Denied: Not Logged In");
  }
  else{
    //If user has access, begin downloading list of information available for computation
    $conn = mysqli_connect(DB_HOST, DB_USER, DB_PSWD, DB_NAME);
    $userEmail = $_SESSION["email"];
    if(!$conn)
      die("Error: Could not connect to database");
    else{
      $q = "SELECT typeGen FROM Datasets WHERE userId = '".$userEmail."' OR public = 1 ORDER BY userId = '".$userEmail."' DESC, public = 1 DESC";
      //$q = "SELECT typeGen FROM Datasets WHERE public = 0 OR public = 1 ORDER BY public = 0 DESC, public = 1 DESC";
      $result = $conn->query($q);
      $sensorTypes = [];
      if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
          array_push($sensorTypes, $row["typeGen"]);
        }
	$sensorTypes = array_unique($sensorTypes);
        //die($resData[0]);
      }
      else
        die("hello");
      $algNames = [];
      foreach($sensorTypes as $name){
        $q = "SELECT algName,purpose FROM Algorithms WHERE signalType = '".$name."' AND (public = 1 OR ownerID = '".$userEmail."') ORDER BY ownerID = '".$userEmail."' DESC, public = 1 DESC";
	$result = $conn->query($q);
        if($result->num_rows > 0){
	  while($row = $result->fetch_assoc()){
	    array_push($algNames, $row["algName"]." (Purpose: ".$row["purpose"].")");
	  }
        }
      }
      $algNames = array_unique($algNames);
      $fileNames = [];
      foreach($sensorTypes as $name){
        $q = "SELECT fileName,streamID,sensorID FROM Datasets WHERE typeGen = '".$name."' AND (userID = '".$userEmail."' OR public = 1) ORDER BY userID = '".$userEmail."' DESC, public = 1 DESC";
	$result = $conn->query($q);
	if($result->num_rows > 0){
	  while($row = $result->fetch_assoc()){
	    $entry = substr($row["fileName"], 18);
	    //array_push($fileNames, $entry." sensor: ".$name." streamID: ".$row["streamID"]);
	    array_push($fileNames, "Sensor ID: ".$row["sensorID"]." | stream ID: ".$row["streamID"]);
	    //array_push($fileNames, $row["sensorID"]);
	  }
	}
      }
      $fileNames = array_unique($fileNames);
    }
    $fileDir  = 'lists/';
    $algDir   = $fileDir.'algList.txt';
    $sensDir  = $fileDir.'sensorList.txt';
    $dataDir  = $fileDir.'dataList.txt';

    $info = array('algs'=>[],'data'=>[],'sens'=>[]);
    $dirs = array('algs'=> $algDir,'data'=>$dataDir,'sens'=>$sensDir);

    //Store info in associative array
    foreach($info as $key=>$value){
      $input = fopen($dirs[$key],'r') or die("Unable to connect to file");
      while(($line = fgets($input)) !== false){
        $info[$key][] = $line;
      }
    }

  }


?><!DOCTYPE html>
<html lan="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Repository</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- jQuery | Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!-- Other Files -->
    <link rel='stylesheet' href='styles/common_css.css'>
    <link rel="stylesheet" href="styles/repository_css.css">

    <script src='scripts/common_js.js'></script>
    <script  src="scripts/repository_js.js" ></script>

  </head>
  <body onload='checkUserID()'>
    <div  id='fill-window'>

      <div class="w-100 dark-theme">
        <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark">
          <a class="navbar-brand" href="https://sites.google.com/a/mail.fresnostate.edu/mri_cloud_sensor/">

            <div class="d-table" id='logoLeft'>
              <div class='d-table-cell align-middle' id='ll-content'>
                <div>
                  NSF
                </div>
                <div>
                  MRI
                </div>
              </div>
            </div>

            <div class="d-table" id='logoRight'>
              <div class="d-table-cell align-middle">
                <div>Cloud Based</div>
                <div class='float-left'>Body Sensor Systems</div>
              </div>
            </div>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBar" aria-controls="navBar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navBar">
            <div class="navbar-nav mr-auto">
              <a class="nav-item nav-link" href="index.html">Home</a>
              <a class="nav-item nav-link" href="reference.html">References</a>
              <a class="nav-item nav-link disabled" id='userDep-Account' href="#">Account <i class="fas fa-lock"></i></a>
              <a class="nav-item nav-link disabled active" id='userDep-Tool'    href="#">Data & Alg Repository <i class="fas fa-lock"></i><span class="sr-only">(current)</span></a>
            </div>
            <div class="navbar-nav">
              <a class='nav-item nav-link' id='userDep' href='login.php'>Login</a>
            </div>
          </div>

        </nav>
      </div>
    </div>


      <!-- Main Body Container -->
      <main class='mt-4' id='push-footer'>
        <div class="container">
          <div class="row">

    	  <!-- SideNav bar -->
	  <div class="col-auto">
	    <div class="list-group" role='tablist' id='sideNav'>
	      <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center active" id='list-data' data-toggle='list' href='#data-content' role='tab' aria-controls="Data">
		Data
	      </a>
	      <a class="list-group-item d-flex justify-content-between align-items-center" id='list-settings' data-toggle='list' href='#settings-content' role='tab' aria-controls="Settings">
		Settings
	      </a>
	    </div>
	  </div>

	  <!-- <div class="tab"> -->
	    <!-- <button class="tablinks" onclick="openTab(event, 'tab1')" id="defaultOpen">Data</button> -->
	    <!-- <button class="tablinks" onclick="openTab(event, 'tab2')">Results</button> -->
	    <!-- <button class="tablinks" onclick="openTab(event, 'tab3')">Analysis History</button> -->
	  <!-- </div> -->

	  <div id="tab1" class="tabcontent">
            <!-- Sensors -->
            <!-- <div class="col-xs-12 col-md-6 col-lg-4"> -->
	    <!-- <div class="col-xs-36 col-md-18 col-lg-12"> -->
	    <div class="w-100">
              <div class="box-group">

                <div class="box-group-title">
                  Sensors
                  <div class="box-group-options">
                    <button type="button" class="btn btn-sm btn-outline-light d-inline-block" data-toggle="collapse" data-target="#sensSearch">
                      <i class='fas fa-search'></i>
                   </button>
                    <button id="sensBtn" class="btn btn-sm btn-outline-light d-inline-light" type="button" data-toggle="collapse" data-target="#sensWrapper">
                      <i class="fas fa-chevron-circle-down"></i>
                   </button>
                  </div>
                </div>
                <div id="sensSearch" class='collapse'>
                  <div class="dark-theme p-2">
                    <input placeholder="Search" onkeyup="updateButtons('#sensWrapper','sensList','.sensors',value)" type="text" class="form-control" placeholder="Username">
                  </div>
                </div>

                <div class='box-group-body'>

                  <div id="sensWrapper" class='collapse show'>
                    <ul id="sensList"class="optionContainer">
                      <!-- Same structure as the others -->
                      <?php foreach($sensorTypes as $name): ?>
                      <li>
                        <button type="button" id="<?php echo $name;?>" class='btn btn-sm btn-list rounded-0 sensors' onclick="updateList('sensors',this);">
                          <?php echo $name; ?>
                        </button>
                      </li>
                      <?php endforeach; ?>
                    </ul>
                  </div>

                </div>
              </div>
              <!--  -->

            </div>

            <!-- Algorithms -->
            <!-- <div class="col-xs-12 col-md-6 col-lg-4"> -->
	    <!-- <div class="col-xs-18 col-md-9 col-lg-6"> -->
	    <div class="w-100">
              <div class="box-group">

                <div class="box-group-title">

                  Algorithms
                  <div class="box-group-options">
                    <button type="button" class="btn btn-sm btn-outline-light d-inline-block" data-toggle="collapse" data-target="#algSearch">
                      <i class='fas fa-search'></i>
                    </button>
                    <button id="algBtn" class="btn btn-sm btn-outline-light d-inline-block" type='button' data-toggle="collapse" data-target="#algWrapper">
                      <i class="fas fa-chevron-circle-down"></i>
                    </button>
                  </div>
                </div>
                <div id="algSearch" class='collapse'>
                  <div class="dark-theme p-2 ">
                    <!-- updateButtons update the list of buttons based on user's search -->
                    <input placeholder="Search" onkeyup="updateButtons('#algWrapper','algList','.algorithms',value)" type="text" class="form-control" placeholder="Username">
                  </div>
                </div>
                <div class='box-group-body'>

                  <div id="algWrapper"class='collapse'>
                    <ul id="algList"class="optionContainer">
                      <!-- Insertion of buttons with php  -->
                      <?php foreach($algNames as $name): ?>
                      <li>
                        <!-- updateList disabled the buttons of the same category and places the selected button in the summary location -->
                        <button type="button" id="<?php echo $name;?>" class='btn btn-sm btn-list rounded-0 algorithms' onclick="updateList('algorithms',this)">
                          <?php echo $name; ?>
                        </button>
                      </li>
                      <?php endforeach; ?>
                    </ul>
                  </div>

                </div>
              </div>
            </div>


            <!-- Data -->
            <!-- <div  class='col-xs-12 col-md-6 col-lg-4'> -->
	    <!-- <div class="col-xs-18 col-md-9 col-lg-6"> -->
	    <div class="w-100">
              <div class="box-group">

                <div class="box-group-title">
                  Data
                  <div class="box-group-options">
                    <button type="button" class="btn btn-sm btn-outline-light d-inline-block" data-toggle="collapse" data-target="#dataSearch">
                      <i class='fas fa-search'></i>
                    </button>
                    <button id="datBtn" class="btn btn-sm btn-outline-light d-inline-block" type="button" data-toggle="collapse" data-target="#dataWrapper">
                      <i class="fas fa-chevron-circle-down"></i>
                    </button>
                  </div>
                </div>
                <div id="dataSearch" class='collapse'>
                  <div class="dark-theme p-2 ">
                    <input placeholder="Search" onkeyup="updateButtons('#dataWrapper','dataList','.data',value)" type="text" class="form-control"  placeholder="Username">
                  </div>
                </div>

                <div class='box-group-body'>

                  <div id="dataWrapper" class="collapse">
                    <ul id="dataList"class="optionContainer" >
                      <!-- Same as algorithm button insertion -->
                      <?php foreach($fileNames as $name): ?>
                      <li>
                        <button type="button" id="<?php echo $name;?>" class='btn btn-sm btn-list rounded-0 data' onclick="updateList('data',this)">
                          <?php echo $name; ?>
                        </button>
                      </li>
                      <?php endforeach; ?>
                    </ul>
                  </div>

                </div>
              </div>
              <!--  -->

            </div>

            <!-- Summary -->
            <!-- <div id="summary" class='col-xs-12 col-md-6 col-lg-4 my-lg-3'> -->
	    <!-- <div id="summary" class="w-25"> -->
	    <div id="summary" class="w-100">
              <div class="box-group">

                <div class="box-group-title">
                  Summary
                </div>

                <div class='box-group-body box-body-border'>

                  <div id="sumWrap" class="container">
                    <!-- Summary of buttons selected inserted below -->
                    <div id="sumSection" class="light-theme">

                    </div>
                    <div class="text-right ">
                      <!-- Button options -->
                      <button type="button" class="btn btn-dark" onclick='runTest()'>Run Test</button>
                      <button  id='compute-btn' class="btn btn-dark" disabled onclick="compute()"> Compute </button>
                    </div>
                  </div>

                </div>
              </div>
              <!--  -->

            </div>

            <!-- Results -->
            <!-- <div id='results' class='col-xs-12 col-lg-8 my-lg-3'> -->
	    <!-- <div id='results' class='w-75'> -->
	    <div id="results" class="w-100">
              <div class="box-group">

                <div class="box-group-title">
                  Analysis Results
                </div>

                <div class='box-group-body box-body-border'>

                  <div id="resultWrap" class="container">
                    <!-- Result text inserted below -->
                    <div id="resSection">
                    </div>
                    <div class="text-right">
                      <button  class="btn btn-dark" onclick="consoleClean()"> Clear </button>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
    </main>
    <!-- Body Ends -->

    <footer id='theFooter'>
      <!-- <div class="container py-3" id='footerUpper'>
        <div class="row">
          <div class="col-12" id='sponsorImgs'>
            <div class="row">
              <div class="col-5 align-self-center ">
                <div class="logoBox">
                  <img id='fsLogo' src="imgs/Fresno_State_Logo.png" alt="College Logo"/>
                </div>
              </div>
              <div class="col-2 align-self-center ">
                <div class="logoBox">
                  <img class='' id='nsfLogo' src="imgs/nsflogo.png" alt='NSF Logo'/>
                </div>
              </div>
              <div class="col-5 align-self-center ">
                <div class="logoBox">
                  <img id='utLogo' src="imgs/ut-dallas-logo.jpg" alt="UT Dallas Logo"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <div class="jumbotron" id="footerLinksWrap">
        <div class="container py-1">
          <div class="row align-items-center mb-2">

            <div class="col">
              <div class="row" id='footerLinks'>

                <div class="col">
                  <a href="http://www.csufresno.edu/">Fresno State</a>
                </div>
                <div class="col">
                  <a href="https://www.nsf.gov/">National Science Foundation</a>
                </div>
                <div class="col">
                  <a href="https://www.utdallas.edu/">UT Dallas</a>
                </div>
                <div class="col">
                  <a href="https://sites.google.com/a/mail.fresnostate.edu/mri_cloud_sensor/">Project Page</a>
                </div>
              </div>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col">
              Copyright &copy; 2016 - 2018 National Science Foundation
            </div>
          </div>
        </div>
      </div>
    </footer>
    </div>
  </body>
</html>

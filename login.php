<?php
  //Login and Registration page
  session_start();
  if(!isset($_SESSION['logged_in'])){
    session_unset();
    $_SESSION['logged_in'] = false;
  }
  // Check If User Already Logged In
  if($_SESSION['logged_in']){
    header("Location:account.php");
    die("Already Logged In");
  }
?><!DOCTYPE html>
<html lan="en-US" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Login</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- jQuery / Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Font Awesome Gallery - https://fontawesome.com/icons?d=gallery <i class='fas fa-<item>'></i> -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!-- Other Files -->
    <link rel='stylesheet' href='styles/common_css.css'>
    <link rel='stylesheet' href='styles/form_css.css'>

    <script src='scripts/common_js.js'></script>
    <script src='scripts/login_register_js.js'></script>
    <script src='scripts/form_js.js'></script>

  </head>
  <body onunload='' onload='checkUserID()'>
    <!-- Same error msg implementation -->

    <div id='error-loc' hidden class="fixed-top alert alert-custom alert-dismissible fade show" role='alert'>
      <div id='error-msgs'>
      </div>
      <button type="button" class="close" onclick="$('#error-loc').attr('hidden',true)">
        <i class='fas fa-times-circle' aria-hidden='true'></i>
      </button>
    </div>

    <!-- TODO: Setup emails on server to allow user to request a temporary new password -->
    <!-- <div  id='fgotPswd-popup' class="alert alert-secondary alert dismissible fade show" role='alert'>
        <button id='fgotPswd-exit' type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class='fas fa-times-circle' aria-hidden='true'></i>
        </button>
      <form class='p-4' id='fgotPswdForm' name='fgotPswdForm' accept-charset="UTF-8" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="POST">
        <input type="text" name="cmd" value="resetPswd" class='d-none'>
        <dl class="form-group">
          <dt class="input-label">
            <label class="form-label mainColorsInv" for="email_fgot_pswd">Enter Email Linked With Lost Password:</label>
          </dt>
          <dd>
            <input type="email" name="email_fgot_pswd" id="email_fgot_pswd" class="form-control form-control-lg" placeholder="Enter Email" autofocus required>
          </dd>
        </dl>
        <button class="btn btn-outline-dark" type="submit">Submit</button>
      </form>
    </div> -->

    <!-- Navagation Bar -->
    <div class="w-100 light-theme">

    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="https://sites.google.com/a/mail.fresnostate.edu/mri_cloud_sensor/">

          <div class="d-table" id='logoLeft'>
            <div class='d-table-cell align-middle' id='ll-content'>
              <div>
                NSF
              </div>
              <div>
                MRI
              </div>
            </div>
          </div>

          <div class="d-table" id='logoRight'>
            <div class="d-table-cell align-middle">
              <div>Cloud Based</div>
              <div class='float-left'>Body Sensor Systems</div>
            </div>
          </div>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBar" aria-controls="navBar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navBar">
          <div class="navbar-nav mr-auto">
            <a class="nav-item nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="reference.html">References</a>
            <a class="nav-item nav-link disabled has-pop-up" id='userDep-Account' href="#">
              Account <i class="fas fa-lock"></i><span class='pop-up-content'>Login Required</span>
            </a>
            <a class="nav-item nav-link disabled has-pop-up" id='userDep-Tool' href="#">Data & Alg Repository <i class="fas fa-lock"></i><span class='pop-up-content'>Login Required</span>
            </a>
          </div>
          <div class="navbar-nav">
            <a class='nav-item nav-link active' id='userDep' href='login.php'>Login<span class="sr-only">(current)</span></a>
          </div>
        </div>

      </nav>
    </div>
    </div>

    <main class="jumbotron rounded-0 dark-theme">
      <div class="d-table w-100">
        <div class="d-table-cell align-middle">
          <div class="formWrap">

            <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-theme" id='loginRegisterTabs'>
              <li class="nav-item">
                <a class="nav-link active" id='login-tab' data-toggle='tab' href="#loginForm" role='tab' aria-controls="loginForm" aria-selected='true'>Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id='register-tab' data-toggle='tab' href="#registerForm" role='tab' aria-controls='registerForm' aria-selected='false'>Register</a>
              </li>
            </ul>

            <!-- Tab Contents -->
            <div class="tab-content">
              <form class="tab-pane fade show active" role='tabpanel' aria-labelledby="login-tab" id='loginForm' name='loginForm' accept-charset="UTF-8" action="" method="">
                <!-- hidden command {'cmd':'login'} -->
                <input class='d-none' type='text' name='cmd'      value='login'>
                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="login_email">Email:</label>
                  </dt>
                  <dd>
                    <input onkeyup="listenForEnter('#submit-loginForm',event)" type="email" name="login_email" id="login_email" class="form-control form-control-lg" placeholder="Enter Email" autofocus>
                    <div class="error-tip" id='login_email-error'></div>
                  </dd>
                </dl>
                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="login_pswd">Password:</label>
                  </dt>
                  <dd>
                    <input onkeyup="listenForEnter('#submit-loginForm',event)" type="password" name="login_pswd" id="login_pswd" class="form-control form-control-lg" placeholder="********">
                    <div class="error-tip" id='login_pswd-error'></div>
                  </dd>
                </dl>
                <button id='submit-loginForm' class="btn btn-outline-dark btn-block btn-lg" type="button" onclick="form_submit('loginForm')">Login</button>
              </form>

              <form class="tab-pane fade" role='tabpanel' aria-labelledby="register-tab" id='registerForm' name='registerForm' accept-charset="UTF-8" action="" method="">
                <!-- id and names dont have to match database field -->
                <!-- {'cmd':'signUp'} -->
                <input class='d-none' type='text' name='cmd'      value='signUp'>
                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="fName">First Name:</label>
                  </dt>
                  <dd>
                    <!-- applyColor applies is-valid or is-invalid classes to change border colors -->
                    <!-- validNoSpace checks if string has no spaces -->
                    <input maxlength="20"  type="text" class="form-control" id="fName" placeholder="First Name" name="fName" autofocus>
                    <!-- any text inside div below appears when input is not valid. ID must be '[id of input]-error'-->
                    <div class="error-tip" id='fName-error'></div>
                  </dd>

                </dl>
                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="lName">Last Name:</label>
                  </dt>
                  <dd>
                    <input maxlength="30" type="text" class="form-control" id="lName" placeholder="Last Name" name="lName">
                    <div class="error-tip" id='lName-error'></div>
                  </dd>
                </dl>
                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="ind">Industry:</label>
                  </dt>
                  <dd>
                    <!-- notBlank checks if input value is non empty, spaces allowed -->
                    <input maxlength="30" type="text" class="form-control" id="ind" placeholder="Industry" name="ind">
                    <div class="error-tip" id='ind-error'></div>
                  </dd>
                </dl>
                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="affil">Affiliation:</label>
                  </dt>
                  <dd>
                    <input maxlength="30" type="text" class="form-control" id="affil" placeholder="Affiliation" name="affil">
                    <div class="error-tip" id='affil-error'></div>
                  </dd>
                </dl>


                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="pos">Position:</label>
                  </dt>
                  <dd>
                    <!-- dropdown -->
                    <select id="pos" class="form-control" name="pos" >
                      <option value='' selected>Choose...</option>
                      <option value='professor'>Professor</option>
                      <option value='researcher'>Researcher</option>
                      <option value='student'>Student</option>
                    </select>
                    <div class="error-tip" id='pos-error'></div>
                  </dd>
                </dl>

                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="field">Field:</label>
                  </dt>
                  <dd>
                    <!-- dropdown -->
                    <select id="field" class="form-control" name="field">
                      <option value='' selected>Choose...</option>
                      <option value='computer science'>Computer Science</option>
                      <option value='biology'>Biology</option>
                      <option value='medical'>Medical</option>
                      <option value='chemistry'>Chemistry</option>
                    </select>
                    <div class="error-tip" id='field-error'></div>
                  </dd>
                </dl>

                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="email">Email:</label>
                  </dt>
                  <dd>
                    <input maxlength="100" type="email" name="email" id="email" class="form-control" placeholder="Enter Email">
                    <div class="error-tip" id='email-error'></div>
                  </dd>
                </dl>
                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label" for="pswd">Password:</label>
                  </dt>
                  <dd>
                    <!-- password checks are done by checking both. Checks if no spaces and not blank and if they are the same -->
                    <input maxlength="100" type="password" name="pswd" id="pswd" class="form-control" placeholder="Enter Password">
                    <div class="error-tip" id='pswd-error'></div>
                  </dd>
                </dl>
                <dl class="form-group">
                  <dt class="input-label">
                    <label class="form-label " for="pswd_check">Re-Type Password:</label>
                  </dt>
                  <dd>
                    <input maxlength="100"  type="password" name="pswd_check" id="pswd_check" class="form-control" placeholder="Enter Password">
                    <div class="error-tip" id='pswd_check-error'>&nbsp;</div>
                  </dd>
                </dl>
                <button id='submit-registerForm' class="btn btn-outline-dark btn-block" type="button" onclick="form_submit_register('registerForm')">Submit</button>
              </form>
            </div>
            <ul class="formOptions">
              <li><a href="#">Forgot Password</a></li>
              <li><a href="index.html">Trial Access</a></li>
            </ul>
          </div>
        </div>
      </div>
    </main>

    <!-- Footer Section -->
    <footer id="theFooter">
      <div class="container" id='footerUpper'>
        <div class="row">
          <div class="col-xs-12 col-md-5" id='contactInfo'>
            <h4 class="text-center title">Contact Info</h4>
            <div class="row">
              <div class="col-4 pr-0">
                <div class='glyph'><i class='fas fa-map-marker'></i></div> Address:
              </div>
              <div class="col-8">
                2576 E. San Ramon
                MS ST 109 Fresno, CA 93704-8039
              </div>
            </div>
            <div class="row">
              <div class="col-4 pr-0">
                <div class='glyph'><i class='fas fa-envelope'></i></div> Email:
              </div>
              <div class="col-8">
                MingLi@csufresno.edu
              </div>
            </div>
            <div class="row">
              <div class="col-4 pr-0">
                <div class='glyph'><i class='fas fa-mobile-alt'></i></div> Phone:
              </div>
              <div class="col-8">
                559-278-4792
              </div>
            </div>
            <div class="row">
              <div class="col-4 pr-0">
                <div class='glyph'><i class='fas fa-fax'></i></div> Fax:
              </div>
              <div class="col-8">
                559-278-4197
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-md-7" id='researchTeam'>
            <h4 class="text-center title">Research Team</h4>
            <div class="row">

              <div class="col-xs-12 col-sm-6 px-0">
                <ul>
                  <li>Dr. Ming Li (CSU Fresno)</li>
                  <li>Dr. B. Prabhakaran (UT Dallas)</li>
                  <li>Dr. Thiru Annaswamy (Dallas VA Medical Center)</li>
                  <li>Joseph Reeves (BS student, CSU Fresno)</li>
                </ul>
              </div>
              <div class="col-xs-12 col-sm-6 px-0">
                <ul>
                  <li>Ai Enkoji (BS student, CSU Fresno)</li>
                  <li>Hunter Reilly (BS student, CSU Fresno)</li>
                  <li>Luis Salazar (BS student, CSU Fresno)</li>
                  <li>Rittika Shamsuddin (PhD student, UT Dallas)</li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12" id='sponsorImgs'>
            <div class="row">
              <div class="col-5 align-self-center">
                <div class="logoBox">
                  <img id='fsLogo' src="imgs/Fresno_State_Logo.png" alt="College Logo"/>
                </div>
              </div>
              <div class="col-2 align-self-center">
                <div class="logoBox">
                  <img class='' id='nsfLogo' src="imgs/nsflogo.png" alt='NSF Logo'/>
                </div>
              </div>
              <div class="col-5 align-self-center">
                <div class="logoBox">
                  <img id='utLogo' src="imgs/ut-dallas-logo.jpg" alt="UT Dallas Logo"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="jumbotron" id="footerLinksWrap">
        <div class="container py-1">
          <div class="row align-items-center mb-2">
            <div class="col">
              <!-- Footer links -->
              <div class="row" id='footerLinks'>
                <div class="col">
                  <a href='login.php#registerForm'>Sign Up</a>
                </div>

                <div class="col">
                  <a href="http://www.csufresno.edu/">Fresno State</a>
                </div>
                <div class="col">
                  <a href="https://www.nsf.gov/">National Science Foundation</a>
                </div>
                <div class="col">
                  <a href="https://www.utdallas.edu/">UT Dallas</a>
                </div>
                <div class="col">
                  <a href="https://sites.google.com/a/mail.fresnostate.edu/mri_cloud_sensor/">Project Page</a>
                </div>
              </div>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col">
              Copyright &copy; 2016 - 2018 National Science Foundation
            </div>
          </div>
        </div>
      </div>

    </footer>
  </body>
</html>

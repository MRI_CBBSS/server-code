// Main JavaScript functions that any page can use

/*
  An additional check to ensure id/class is formatted
  correctly before doing $('#id') vs document.getElementById('id');

  Prepends or removes # or . to a given value
  Option: 'add' or 'remove' | Type:'id' or 'class'
  Does nothing if value already formatted as requested
  format_indicator('add','id','#someId') returns '#someId'
  format_indicator('add','id','.someId') returns '#someId'
*/
function format_indicator(option,type,value){
  var ind = '?';
  var other = '?';
  type=='id'?ind='#':ind='.';
  ind=='.'?other='#':other='.';
  if(option == 'add'){
    //if indicator not in value
    if(value.indexOf(ind) == -1){
      //if other type indicator is in front, replace it
      if(value.indexOf(other)==0){
        return value.replace(other,ind);
      }
      //just append indicator if needed
      else{
        return ind + value;
      }
    }
  }
  else{
    if(value.indexOf(ind) == 0){
      return value.replace(ind,'');
    }
  }
  return value;
}

//Triggers buttonID if event.key entered is the Enter key
function listenForEnter(buttonID,e){
  buttonID = format_indicator('add','id',buttonID);
  var code = e.keyCode || e.which;
  if(code === 13){
    $(buttonID).click();
    $(buttonID).button('toggle');
    $(document).one('click',function(){
      $(buttonID).button('toggle');
    });
  }
}

// Displays error messages on top of the screen
// errorList = ['Password incorrect','Other issues',etc.]
function displayErrors(errorList){
  var location    = $('#error-loc');
  var msgsLoc     = $('#error-msgs');
  var strongError = '<strong>Error</strong>';

  msgsLoc.html('');
  location.removeAttr('hidden');

  for (var i = 0; i < errorList.length; i++) {
    msgsLoc.append(strongError +' - '+ errorList[i]);
    //If error is not the last one, add a <br> element
    if(i != errorList.length-1){
      msgLoc.append('<br>');
    }
  }
}

//Toggle readonly property of a given class name
//Passed button is the button that toggles the readonly property
function togReadOnly(className,button){
  className = format_indicator('add','class',className);
  var bool  = !$(className).attr('readonly');
  var label = bool?'Edit':'Cancel';
  $(button).html(label);
  $(className).each(function(){
    $(this).prop('readonly',bool);
  });
  return bool;
}


//Sends a GET request to see if a user is logged in
function checkUserID(isHomePage=false){
  $.get('serverFiles/requestHandler.php',{"cmd":"loadID"},function(jsonStrResult,status){
    var userID = 'unknown';
    if(status == 'success'){
      var jsonObj = JSON.parse(jsonStrResult);
      userID  = jsonObj['email'];
    }
    else{
      alert('Could Not Verify User');
    }
    //Update the page if a user is logged in
    LoggedIn_Updates(userID,isHomePage);
  });
}

//If user is logged in modify the page
//Enables links to account and repository
//login become logout, etc.
function LoggedIn_Updates(userName,isHomePage){
  var link = $('#userDep');
  if(userName != 'unknown'){
    //If user signed in

    //If page calling the function is the isHomePage
    //hide certain elements
    if(isHomePage){
      $('#homeLogin').addClass('d-none');
      //Update Home Introduction to take up the entire width
      if($('#homeIntro').length > 0){
        document.getElementById('homeIntro').className='col';
      }
    }
    //Remove any links not necessary when logged in
    $('.userDep-Links').remove();

    //Change login link to logout
    link.removeClass('active');
    link.click(function(){signOut();return false;});
    link.prop('href','javascript:signOut();return false;');
    link.html("Logout")

    //Active account profile link
    $('#userDep-Account').removeClass('disabled');
    $('#userDep-Account').prop('href','account.php');
    $('#userDep-Account').html('Account');

    //Activate repository link
    $('#userDep-Tool').removeClass('disabled');
    $('#userDep-Tool').prop('href','repository.php');
    $('#userDep-Tool').html('Data & Alg Repository');
  }
}



//Used in conjunctino with togDisabled to modify the value of
//the btn based on whether the given className is disabled
function editBtnToggle(btn,className){
  className = format_indicator('add','class',className);
  var bool  = !$(className).attr('disabled');
  console.log(bool);
  var label = bool?'Edit':'Cancel';
  $(btn).html(label);
}

//Toggles the disable property of all elements that share the className
function togDisabled(className){
  className = format_indicator('add','class',className);
  var bool  = !$(className).attr('disabled');
  $(className).each(function(){
    $(this).prop('disabled',bool);
  });
}

//Used to have a user sign out through a POST request
function signOut(){
  $.post('serverFiles/requestHandler.php',{"cmd":"signOut"},function(jsonStrResult,status){
    if(status == 'success'){
      if(jsonStrResult == 'success'){
        //Upon signing out, place user at the index page
        window.location='index.html';
      }
    }
    else{
      alert('Error Signing Out');
    }
  },'text');
}

// Returns a String representation of the current time
// 'hr:min:seconds AM/PM'
function getTime(){
  var date = new Date();
  var hour = date.getHours();
  var mins = date.getMinutes();
  var secs = date.getSeconds();
  var ampm = hour<=12?'AM':"PM";
  var time = (hour<12?hour:(hour==12?12:(hour-12)))+':'+
             (mins<10?'0'+mins:mins)+':'+
             (secs<10?'0'+secs:secs)+' '+ampm+' - ';
  return time;
}

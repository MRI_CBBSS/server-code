// Functions specific to the account page

// For actions that require the page to be loaded beforehand
$(document).ready(function(){
  // Required to push down the footer towards the bottom of the screen
  $('#push-footer').css("padding-bottom",$("#theFooter").outerHeight()+30);
  checkApprovedAlgs();

  // Required for Uploading files - Account Page - Uploads tab
  // ------------- ALG -----------------

  // Triggers hidden input file
  $("#uploadFileLauncher-Alg").click(function(){
    $("#uploadFile-Alg").focus().trigger('click');
  });
  // Submit button that submits the hidden input through a form submission
  $("#submitBtn-Alg").click(function(){
    if($('#uploadFile-Alg')[0].files.length != 0){
      $('#fileUploadForm-Alg').submit();
    }
    else{
      alert('Please Select A File First');
    }
  });
  // Update the label once user selects a file
  $('#uploadFile-Alg').change(function(){
    $('#alg-label').html($('#uploadFile-Alg')[0].files[0].name);
  });
  //--------------- DATA -----------------

  // Triggers hidden input file
  $("#uploadFileLauncher-Data").click(function(){
    $("#uploadFile-Data").focus().trigger('click');
  });

  // Submit button that submits the hidden input through a form submission
  $("#submitBtn-Data").click(function(){
    if($('#uploadFile-Data')[0].files.length != 0){
      $('#fileUploadForm-Data').submit();
    }
    else{
      alert('Please Select A File First');
    }
  });
  // Update the label once user selects a file
  $('#uploadFile-Data').change(function(){
    $('#data-label').html($('#uploadFile-Data')[0].files[0].name);
  });

});
// Update footer's location on resize
$(window).resize(function(){
  $('#push-footer').css("padding-bottom",$("#theFooter").outerHeight()+30);
});

// Initiates downloading of a file through a GET request
function downloadFile(path){
  var jsonObj = {'filePath':path, 'cmd':'downloadFile'};
  $.get('serverFiles/requestHandler.php',jsonObj,function(jsonStrResult,status){
    if(status == 'success'){
      console.log('download success');
    }
    else{
      alert('Error Downloading');
    }
  });
}


// Create a Post request to update the users password
$('#submit-update').on('click',function(){
  var v1 = $('#new-pswd').val();
  var v2 = $('#new-pswd-check').val();
  var v3 = $('#og-pswd').val();
  if(v1 != '' &&  v3 != '' && v1 == v2) {
    var formJson = serializeForm('#pswdUpdate-form');
    $.post('serverFiles/requestHandler.php',formJson,function(jsonStrResult,status){
      if(status == 'success'){
        alert('Password Updated');
      }
      else{
        alert('Error Updating Password');
      }
    });
  }
});

// Initiates deletion of file through a GET request
function removeFile(path){
  var jsonObj = {'filePath':path,'cmd':'removeFile'};
  $.get('serverFiles/requestHandler.php',jsonObj,function(jsonStrResult,status){
    if(status = 'success'){
	window.location = 'account.php';
    }
    else{
        alert('Deletion Failed');
    }
  });
}

// TODO: Implementation unknown - should display the algorithms that have been approved
function checkApprovedAlgs(){
  var e = $('#approved-algs-loc');
  if(e.children().length == 0){
    e.html('<td>No Algorithms Have Been Approved</td>');
  }
}

// Assigna values to checked checkboxes inside a specified form
// Submits the form if all checked values are formatted correctly
function assignCheckVals(formID){
  formID = format_indicator('add','id',formID);
  var noErrors = true;
  var selector = formID + ' input:checked';

  $(selector).each(function(){
    var valid   = true;
    var info    = $(this).attr('id').split('-');
    var inputID = info[1];
    //Conditional for id's with multiple dashes: #check-account-data-something
    if(info.length > 2){
      for(var i=2;$i < info.length; i++) {
        inputID = inputID+'-'+info[i];
      }
    }
    console.log(info+':'+inputID);

    inputID = format_indicator('add','id',inputID);
    //validate specific data
    switch(inputID){
      case '#firstName':
        valid = validNoSpace($(inputID).val(),true,inputID);
        break;
      case '#lastName':
        valid = validNoSpace($(inputID).val(),true,inputID);
        break;
      case '#industry':
        valid = notBlank($(inputID).val(),true,inputID);
        break;
      case '#affiliation':
        valid = notBlank($(inputID).val(),true,inputID);
        break;
      case '#position':
        valid = notBlank($(inputID).val(),true,inputID);
        break;
      case '#field':
        valid = notBlank($(inputID).val(),true,inputID);
        break;
      case '#email':
        valid = validEmail($(inputID).val(),true,inputID);
        break;
      default:
        valid = false;
        alert('assignCheckVals failed');
    }
    if(!valid){
      applyColor(inputID,false);
    }
    noErrors = noErrors && valid;
  });

  // If No errors found, assign values and submit the form
  if(noErrors){
    $(selector).each(function(){
      var info    = $(this).attr('id').split('-');
      var inputID = info[1];
      //Conditional for id's with multiple dashes: #check-account-data-something
      if(info.length > 2){
        for(var i=2;$i < info.length; i++) {
          inputID = inputID+'-'+info[i];
        }
      }
      inputID = format_indicator('add','id',inputID);
      //Format the values [name]-[name's value]
      $(this).val($(inputID).attr('name') + '-' + $(inputID).val());
    });
    $(formID).submit();
  }

}

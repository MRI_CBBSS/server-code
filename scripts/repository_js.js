
// Functions specific to the repository page

// For actions that require the page to be loaded beforehand
$(document).ready(function(){
  // Required to push down the footer towards the bottom of the screen
  $('#push-footer').css("padding-bottom",$("#theFooter").outerHeight()+30);
  // Display Initial Console Message
  consoleInit();
  // Onclick for summary buttons
  $("body").on("click",".removeOption",function(){
    var element = document.getElementById(this.id.slice(0,-4));
    var classes = element.className.split(" ");

    // Remove Summary Button Choice
    this.remove();

    // Enable button choices for removed button type (alg/sensor/data)
    // Categorical class name must be the last one
    // class='btn btn-theme removeOption algorithm'
    $("."+classes[classes.length-1]).prop("disabled",false);

    // Disable compute button if requirements to run algorithm are not met
    // COMBAK: should depend on more detailed info besides selection count
    $('#compute-btn').prop('disabled',true);
  });
});
//Update footer's location on resize
$(window).resize(function(){
  $('#push-footer').css("padding-bottom",$("#theFooter").outerHeight()+30);
});

// Simple test run computation
function runTest(){
  $('#resSection').html('Computing...');
  $.get('serverFiles/requestHandler.php',{'cmd':'runTest'},function(strResult,status){
    if(status == 'success'){
      $('#resSection').html(strResult);
    }
    else{
      alert('Test Failed');
    }
  });
}
// Called when search bar used - Displays matching buttons
function updateButtons(wrapper,locID,className,search){
  if(search != ''){
    var location   = document.getElementById(locID);
    var successors = location.children;
    var found      = false;

    search = search.toLowerCase();

    for(var i = 0; i < successors.length; ++i){
      var id = successors[i].children[0].id;
      //Display results if found
      if(id.toLowerCase().indexOf(search) == 0){
        document.getElementById(id).style.display = 'block';
        found = true;
      }
      else{
        document.getElementById(id).style.display = 'none';
      }
    }
    found?$(wrapper).collapse('show'):$(wrapper).collapse('hide');
  }
  else{
    $(wrapper).collapse('hide');
    $(className).css({display:'block'});
  }
}

//Disables buttons of the class className and makes a summary button
function updateList(className,e){
  var selector = format_indicator('add','class',className);
  $(selector).prop("disabled",true);
  $('#sumSection').html(
    $('#sumSection').html() +
    "<button id='"+e.id+"_cpy' class='btn btn-outline-dark btn-sm removeOption "+className +"'>"+
    e.id+" <i class='fas fa-times-circle'></button>"
  );
  var btnName = "none";
  var oldBtn = "none";
  if(className=="sensors"){
    btnName = "algBtn";
    oldBtn = "sensBtn";
  } else if(className == "algorithms"){
    btnName = "datBtn";
    oldBtn = "algBtn";
  } else if(className == "data"){
    btnName = "datBtn";
  }
  if(btnName != "none")
    document.getElementById(btnName).click();
  if(oldBtn != "none")
    document.getElementById(oldBtn).click();
  // COMBAK: update conditional check to something more detailed once requirements of data/alg/sensors are known
  if(document.getElementById('sumSection').children.length == 3){
    $('#compute-btn').prop('disabled',false);
  }
}

// Places summary section into a json object
function summaryToJson(){
  var buttons     = document.getElementById('sumSection').children;
  var sendingData = {"cmd":"runAlg"};

  for(var i = 0; i < buttons.length; ++i){
    //remove '_cpy' off id='someAlgName_cpy'
    var id = buttons[i].id.slice(0,-4);
    var element  = document.getElementById(id);
    var classes  = element.className.split(" ");

    // Make plural to singular: algorithms -> algorithm
    // Categorical class must be the last one
    // class='btn btn-outline-dark removeOption algorithms'
    var category = classes[classes.length-1];
    if(category.slice(-1) == 's'){
      category = category.slice(0,-1);
    }
    // Remove any returns
    sendingData[category] = id.replace(/\r?\n|\r/g,"");
  }
  return sendingData;
}

function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display="none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for(i = 0; i < tablinks.length; i++){
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Sends data over to the server
function compute(){
  //alert($('#sumSection').children()[0].textContent);
  // COMBAK: update conditional check to something more detailed once requirements of data/alg/sensors are known
  if($('#sumSection').children().length == 3){
    var jsonObj = summaryToJson();
    consoleWrite("Computing...");
    consoleScrollBottom();
    /*$.get('serverFiles/handleRequests.php',jsonObj,function(jsonStrResult,status){
      if(status == 'success'){
	var returnObj = JSON.parse(jsonStrResult);
        var resultText = "Done";
        consoleWrite(resultText);
      }
      else{
        consoleWrite("Something Went Wrong...");
      }
    });
    */
    var sensor = $('#sumSection').children()[0].textContent;
    sensor = sensor.replace(/\s/g, '');
    var alg = $('#sumSection').children()[1].textContent;
    alg = alg.substr(0,alg.indexOf(' '));
    var btn3 = $('#sumSection').children()[2].textContent.split(' ');
    sensID = btn3[2];
    strmID = btn3[6];
    //alert("../websockz/chat/");
    $.post('serverFiles/requestHandler.php',{'cmd':'runAlg','sensor':sensor,'alg':alg,'sensID':sensID,'strmID':strmID}, function(strResult,status){
      if(status=='success'){
        $('#resSection').html(strResult);
      }
      else{
        alert('Test Failed');
      }
    });
  }
  else{
    consoleWrite('Please Complete Your Selection');
    consoleScrollBottom();
  }
  consoleNewSection();
}


//----Console Functions----//

//Initial Hello Message
function consoleInit(){
  consoleClean();
  consoleWrite("Welcome to the CSUF tool! - Functionality In Progress");
}
//Write given text to console
function consoleWrite(txt){
  var cons = $('#resSection');
  cons.html(consoleHTML()+getTime()+txt+"<br>");
}
//Empty the console
function consoleClean(){
  $('#resSection').html('');
}
//Returns the current elements inside of the console
function consoleHTML(){
  return document.getElementById('resSection').innerHTML;
}
//Places a divider
function consoleNewSection(){
  $('#resSection').html(consoleHTML()+"====================================<br>")
}
//Scrolls the console to the bottom
function consoleScrollBottom(){
  $('#resSection').animate({scrollTop:$('#resSection').prop('scrollHeight')},'fast');
}

document.getElementById("defaultOpen").click();

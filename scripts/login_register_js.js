//On page load
$(document).ready(function(){
  readURL();
  var regIdOnKeyup = ['#fName','#lName','#ind','#affil','#email','#pswd','#pswd_check'];
  for(var i = 0; i < regIdOnKeyup.length; ++i){
    $(regIdOnKeyup[i]).keyup(function(e){listenForEnter('#submit-registerForm',e);})
  }
  $('#fName').keyup(function(e){   validNoSpace($(this).val(),true,'#fName'); });
  $('#lName').keyup(function(e){   validNoSpace($(this).val(),true,'#lName'); });
  $('#email').keyup(function(e){   validEmail($(this).val(),true,'#email');   });
  $('#affil').keyup(function(e){   notBlank($(this).val(),true,'#affil');     });
  $('#ind').keyup(function(e)  {   notBlank($(this).val(),true,'#ind');       });

  $('#pos').change(function(e){
    listenForEnter('#submit-registerForm',e);
    notBlank($(this).val(),true,'#pos');
  });
  $('#field').change(function(e){
    listenForEnter('#submit-registerForm',e);
    notBlank($(this).val(),true,'#field');
  });
  $('#pswd').keyup(function(e){
    pswdCheck('pswd','pswd_check',true);
    pswdCheck('pswd_check','pswd',true);
  });
  $('#pswd_check').keyup(function(e){
    pswdCheck('pswd_check','pswd',true);
    pswdCheck('pswd','pswd_check',true);
  });
});

//Tiggers nav tabs in login page based on url
//bsncloud.csufresno.edu/login.php#registerForm
function readURL(){
  var url = document.location.toString();
  if (url.match('#')) {
      $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
  }
}

<?php
  // Contains upload file function used in requestHandler.php

  // Required for getUserPath()
  include 'databaseFunctions.php';


  function upload_file($fileType,$fileTemp,$fileName,$fileSize,$userEmail){
    $fileList = explode(" ",$fileName);
    $fName = "";

    // Concatenate file list
    for($i = 0, $j = count($fileList); $i < $j; $i++){
      $fName.=$fileList[$i];
    }

    $base = getUserPath($userEmail);// userData/userId
    $typeDir = $base.'/'.$fileType; // base/userData/Algorithms
    $dir = $typeDir.'/'.$fName;     // base/userData/Algorithms/fileName.txt

    if(!file_exists($base)){mkdir($base);}
    if(!file_exists($typeDir)){mkdir($typeDir);}

    $info = pathinfo($dir);
    $ext  = $info['extension'];
    $name = $info['filename'];

    // Currently Limited to 300 KB
    if($fileSize < 300000){
      if(file_exists($dir)){
        //Make a copy fileName_2.txt
        $i = 1;
        $name .= "_$i";
        $dir = $typeDir.'/'.$name.'.'.$ext;
        //If still exists, incriment to fileName_3.txt and so forth
        while(file_exists($dir)){
          $i += 1;
          $name = substr($name,0,-1).$i;
          $dir  = $typeDir.'/'.$name.'.'.$ext;
        }
      }
      // Attempt to move
      if(move_uploaded_file($fileTemp,$dir)){
        echo json_encode(['success'=>'File '.basename($fileName).' has been uploaded']);
      }
      // Move Failed
      else{
        echo json_encode(['error'=>"File Transfer Failed"]);
      }
    }
    else{
      echo ['error'=>'File Too Large'];
    }
  }
?>

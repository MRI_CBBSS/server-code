<?php
	// Standalone - Handles file downloads

	if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['filePath'])){
		// Files Currently stored in serverFiles and can be viewed by anyone by knowing the link
		$file = '/opt/stack/horizon/.blackhole/serverFiles/'.str_replace("'",'',$_GET['filePath']);

		//If file found
		if(file_exists($file)){
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires:0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: '.filesize($file));
			readfile($file);

			exit;
		}
		else{
			echo 'File doesnt exist';
			header('Location: ../account.php');
		}
	}
?>

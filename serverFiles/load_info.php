<?php
//Not needed anymore
  function load_info($fileName){
    $fileDir = '../lists/';
    switch($fileName){
      case 'algorithms':
        $fileDir .= 'algList.txt';
        break;
      case 'sensors':
        $fileDir .= 'sensorList.txt';
        break;
      case 'data':
        $fileDir .= "dataList.txt";
        break;
      default:
        $fileDir = $fileName;
    }
    if($fileDir != $fileName){
      $algs = [];
      $data = fopen($fileDir,'r') or die("Unable to connect to file");
      while(($line = fgets($data)) !== false){
        $algs[] = $line;
      }
      return json_encode($algs);
    }
    else{
      return json_encode(["error"=>"Error in reading from file:".$fileDir]);
    }
  }
?>

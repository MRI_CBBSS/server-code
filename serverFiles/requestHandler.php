<?php
  include "database.php";
  //=========================================================
  // Main php file that handles most requests
  //=========================================================

  // Create/Resume current session
  session_start();

  //***************************** Handle POST Requests *****************************//
  if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if a command is given
    if(!empty($_POST['cmd'])){

      switch($_POST['cmd']){

        //====================== Update Account Information ======================
        case 'updateAccount':
          include 'databaseFunctions.php';
          $info = array();

          $tempEmail = $_SESSION['email'];
          //Check if updateValues is given - checked checkboxes
          if(isset($_POST['updateValues'])){
            $values = $_POST['updateValues'];

            foreach($values as $value){
              //'id-value'
              $info  = explode('-',$value);
              $id    = $info[0];
              $value = $info[1];
              //conditional check for any string that has dashes inside as well
              //lastName-something-cool => lastname something-cool
              if(count($info) > 2){
                for ($i=2;$i < count($info); $i+=1) {
                  $value .= '-'.$info[$i];
                }
              }
              // If updating email, store it
              if($id == 'email'){
                $tempEmail = $value;
              }
              $info[$id] = $value;

            }
            // Update all information
            db_updateInfo('Accounts',$info,['email',$_SESSION['email']]);
            // Update new email
            $_SESSION['email'] = $tempEmail;
            header('location:../account.php');
          }
          //No Checked Checkboxes
          else{
            echo "<script type='text/javascript'>alert('No Selected Values To Update'); window.location='../account.php';</script>";
          }
          break;

        //====================== Update User's Password ======================
        case 'updatePswd':
          include 'databaseFunctions.php';

          // Check if no original password given
          if (!isset($_POST['og-pswd']) || empty($_POST['og-pswd'])) {
            echo json_encode(['errorIDs'=>['og-pswd'],'success'=>false,'errors'=>['Please Enter Your Password']]);
          }
          // Check if new password is given
          elseif(isset($_POST['new-pswd']) && isset($_POST['new-pswd-check'])){
            $newPswd = $_POST['new-pswd'];
            $chkPswd = $_POST['new-pswd-check'];
            $ogPswd  = $_POST['og-pswd'];

            // Make sure that the new password matches the password check
            if(!empty($newPswd) && $newPswd == $chkPswd){
              // Get Users Password from the database
              $db_data = db_getInfo('Accounts',['password'],['email',$_SESSION['email']]);
              // Verify that the passwords do match
              if(password_verify($ogPswd,$db_data['password'])){
                db_updateInfo('Accounts',['password'=>$newPswd],['email',$_SESSION['email']]);
              }
              else{
                echo json_encode(['errorIDs'=>['og-pswd'],'success'=>false,'errors'=>['Incorrect Password']]);
              }
            }
            else{
              echo json_encode(['errorIDs'=>['new-pswd','new-pswd-check'],'success'=>false,'errors'=>['Passwords Do Not Match']]);
            }
          }
          else{
            echo json_encode(['errorIDs'=>[],'success'=>false,'errors'=>['Missing Values']]);
          }
          break;
        //====================== Run Algorithm ======================//
        case 'runAlg':
	  include "databaseFunctions.php";
          // TODO: Implementation unknown
          // include "run_alg.php";
	  $sensor = $_POST['sensor'];
	  $alg = $_POST['alg'];
	  $sensID = $_POST['sensID'];
	  $strmID = $_POST['strmID'];
	  $fPath = "/opt/stack/horizon/.blackhole/websockz/chat/";
	  //pull rest of needed data from repository.js and perform analysis here
	  if(!empty($sensor) && !empty($alg) && !empty($sensID) && !empty($strmID)){
	    //echo $strmID;
	    //echo $alg;
	    $dbAlg_data = db_getInfo('Algorithms',['filePath'],['algName',$alg]);
	    $fPath .= $dbAlg_data['filePath'];
	    $runner = "python ".$fPath." ".$sensID." ".$sensor." ".$strmID;
	    $command = escapeshellcmd($runner);
	    $result = shell_exec($command);
	    echo $result;
	  }
          else
	    echo "hello";
          break;

        //====================== User login ======================//
        case 'login':
          include "login_backend.php";

          $response = user_login($_POST['login_email'],$_POST['login_pswd']);
          echo json_encode($response);
          break;
        //====================== User Registration ======================//
        case 'signUp':
          include "signUp_backend.php";

          $response = user_signUp($_POST);
          echo json_encode($response);
          break;
        //====================== Sign Out ======================//
        case 'signOut':
          include 'databaseFunctions.php';
          db_updateInfo('Accounts',['active'=>0],['email',$_SESSION['email']]);
          session_unset();
          $_SESSION['logged_in'] = false;
          echo 'success';
          break;

        //====================== File Upload ======================//
        case 'uploadFile':
          // Only logged in users can upload files
          if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){
            include "upload_file.php";

            // Should be the name of the input that has the file
            $fileCmd  = 'uploadFile';
            $fileType = $_POST['fileType'];

            if($fileType == '0'){
              $fileType = "Algorithms";
              $fileCmd .= '-Alg';
            }
            elseif($fileType == '1'){
              $fileType = "Datasets";
              $fileCmd .= "-Data";
            }
            $fileTemp  = $_FILES[$fileCmd]['tmp_name'];
            $fileName  = $_FILES[$fileCmd]['name'];
            $fileSize  = $_FILES[$fileCmd]['size'];
            $userEmail = $_SESSION['email'];

            upload_file($fileType,$fileTemp,$fileName,$fileSize,$userEmail);
 	          header('location: ../account.php');
          }
          // User is not logged in
          else{
            echo "<script type='text/javascript'>alert('Access Required');</script>";
            header("location: ../index.html");
          }
          break;
        //====================== Command Not Known ======================//
        default:
          echo json_encode(['error'=>true,'errorMsg'=>'Command Not Found']);
      }
    }
    // No Command Given
    else{
      echo json_encode(['error'=>true,'errorMsg'=>'No Command Given']);
    }
  }

  //***************************** Handle GET Requests *****************************//
  elseif($_SERVER["REQUEST_METHOD"] == "GET"){
    // Check if Command given
    if(!empty($_GET['cmd'])){

      switch($_GET['cmd']){

        //====================== Deletion of a Uploaded File ======================//
      	case 'removeFile':
      	  if(isset($_GET['filePath'])){
       	    $file = '/opt/stack/horizon/.blackhole/serverFiles/'.$_GET['filePath'];
      	    if(file_exists($file)){
      	      unlink($file);
      	    }
      	  }
       	  break;

        //====================== Simple Test Run ======================//
        case 'runTest':
          $command = escapeshellcmd('python /opt/stack/horizon/.blackhole/testFiles/statEx.py');
          $output  = shell_exec($command);
          echo $output;
          break;

        //====================== Get User's email id ======================//
        case 'loadID':
          // logged in user
          if(isset($_SESSION['email'])){
            echo json_encode(["email"=>$_SESSION['email']]);
          }
          // not logged in
          else{
            echo json_encode(['error'=>true,"email"=>"unknown","errorMsg"=>"email not set"]);
            $_SESSION['logged_in'] = false;
          }
          break;

        //====================== Command Not Known ======================//
        default:
          echo json_encode(['error'=>true,"errorMsg"=>"Command Not Found"]);
      }
    }
    // No Command Given
    else{
      echo json_encode(['error'=>true,'errorMsg'=>'No Command Given']);
    }
  }

 ?>

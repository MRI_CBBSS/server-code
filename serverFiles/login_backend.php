<?php
  // Contains Login function used in requestHandler.php
  // Requires normalize(), db_getInfo(), and db_updateInfo()
  include "databaseFunctions.php";

  /*
  * Function that logs a user in
  * Requires form names: login_email and login_pswd to display errors
  * $result looks something like this when returned:
  *   {'errorCount':2, 'errors':['something','other'],'errorIDs':['loginID,otherID'],'success'=false}
  * errorIds are used in javascript to add red borders to the input elements that contain errors
  * errors are displayed on the html page using bootstrap alerts
  */
  function user_login($email,$pswd){
    $user_email = $user_pswd = "";
    $result     = array('errorCount'=>0,'errors'=>array(),'errorIDs'=>array(),'success'=>false);
    $noError    = true;

    // Begin Validation Checks
    if(empty($email)){
      $noError = false;
      $result['errors'][] = "Email Address Entered Is Empty";
      $result['errorCount'] += 1;
      $result['errorIDs'][]='login_email';
    }
    else{
        $user_email = normalize($email);
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $noError = false;
            $result['errors'][] = "Invalid Email Address";
            $result['errorCount'] += 1;
            $result['errorIDs'][]='login_email';
        }
    }
    if(empty($pswd)){
        $noError = false;
        $result['errors'][] = "Password Entered Is Empty";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='login_pswd';
    }
    else{
        $user_pswd = normalize($pswd);
    }

    //If no errors were found
    if($noError){
        //Get the user's password from the database
        $db_data   = db_getInfo('Accounts',['password'],['email',$user_email]);
        //Check if the password used to login matches the stored password
        if(count($db_data) != 0 && password_verify($user_pswd,$db_data['password'])){
          $request  = array('firstName','lastName');

          //Access more informatio to save in the user's session
          $userInfo = db_getInfo('Accounts',$request,['email',$user_email]);

          $_SESSION['logged_in'] = true;
          $_SESSION['fName'] = $userInfo['firstName'];
          $_SESSION['lName'] = $userInfo['lastName'];
          $_SESSION["email"] = $user_email;

          //Modify database to show the user is currently online
          // COMBAK: Not sure if necessary yet - perhaps use timestamps and intervals instead
          db_updateInfo('Accounts',['active'=>1],['email',$user_email]);

          //sucess
          if($result['errorCount'] == 0){
            $result['success'] = true;
          }
        }
        else{
          $result['errors'][] = "Email/Password Does Not Match";
          $result['errorIDs'][]='login_pswd';
          $result['errorIDs'][]='login_email';
          $result['errorCount'] += 1;
        }
      }
      return $result;
    }
?>

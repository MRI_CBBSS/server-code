<?php
  // File containing all database related functions

  // Databse Info
  define('DB_HOST', 'localhost' );
  define('DB_NAME', 'csufml_db' );
  define('DB_USER', 'matt');
  define('DB_PSWD', 'CSci@5657'); // Never got around to changing my default given password

  // Cleans up given data
  function normalize($data){
    //removes whitespace and other predefined characters from both sides of a string.
    $data = trim($data);
    //removes backslashes
    $data = stripslashes($data);
    /*converts some predefined characters to HTML entities.
      & to &amp;
      " to &quot;
      ' to &#039;
      < to &lt;
      > to &gt;
    */
    $data = htmlspecialchars($data);
    return $data;
  }

  #Checks if given parameters already exists in the given table name
  #associative array for checking multiple fields
  #Ex: db_exists('Accounts',['password' => 'somePass', 'username' => 'Luis']);
  function db_exists($table,$parameters){
    //Connect
    $conn = mysqli_connect(DB_HOST,DB_USER,DB_PSWD,DB_NAME);
    if(!$conn){
      echo("db_exists: Conection failed due to: ".mysqli_connect_error());
    }
    else{
      $condition = "";
      //Concatenate given parameters into a string
      foreach($parameters as $key => $value){
        $key = mysqli_real_escape_string($conn,$key);
        $value = mysqli_real_escape_string($conn,$value);
        $condition .= $key."='$value' AND ";
      }
      //Remove the final ' AND '
      $condition = substr($condition,0,-5);
      $query = 'SELECT * FROM '.$table.' WHERE '.$condition.' LIMIT 1';
      $result = mysqli_query($conn,$query);

      if($result){
        if(mysqli_num_rows($result)>0){
          mysqli_close($conn);
          return True;
        }
      }
      else{
        echo("db_exists - Query Failed due to: ". mysqli_error($conn));
      }
      mysqli_close($conn);
      return False;
    }
  }

  # Insertion of data into a table
  # Parameters are in an associative array ["email"=>"school@school.com",...]
  function db_insert($table,$parameters){
    $conn = mysqli_connect(DB_HOST,DB_USER,DB_PSWD,DB_NAME);

    if(!$conn){
      echo "Insert Data: Connection Failed: ".mysqli_connect_error();
    }

    //Query construction
    $dataQuery = 'INSERT INTO '.$table.'(';
    $keys = "";
    $values = ' VALUES (';
    foreach($parameters as $key => $value){
      if($key == 'password'){
        $value = password_hash($value,PASSWORD_BCRYPT);
      }
      if($key != 'id'){
        $key     = mysqli_real_escape_string($conn,$key);
        $value   = mysqli_real_escape_string($conn,$value);
        $keys   .= "$key,";
        $values .= "'$value',";
      }
    }
    #remove final commas and append ")"
    $keys = substr($keys,0,-1).')';
    $values = substr($values,0,-1).')';

    $dataQuery .= $keys.$values;
    if(!mysqli_query($conn,$dataQuery)){
      echo("Insertion Failed: ". mysqli_error($conn));
      mysqli_close($conn);
      return False;
    }
    mysqli_close($conn);
    return True;
  }

  /*
    Retrieving data from the database
    Selectors - list of all requested data - ['password','email',...]
    target - No tuples in php so target is an array containing 2 items
             First item should be the target's field name - 'email'/'userId'/etc.
             Second item is the value of the target field - 'someEmail@email.com'/'0'/etc.
             ['email','someEmail@something.com']
  */
  function db_getInfo($table,$selectors,$target){
    $data = [];
    $conn = mysqli_connect(DB_HOST,DB_USER,DB_PSWD,DB_NAME);

    if(!$conn){
      echo "Insert Data: Connection Failed".mysqli_connect_error().PHP_EOL;
    }

    #Construct Query
    $query = 'SELECT ';
    for($i = 0; $i < count($selectors);$i++){
        $query .= mysqli_real_escape_string($conn,$selectors[$i]).",";
    }
    $query = substr($query,0,-1) . " FROM $table ". "WHERE $target[0] = '$target[1]'";
    $result = mysqli_query($conn,$query);

    if(!$result){
        echo('Get Info Failed: '.mysqli_error($conn));
    }
    elseif(mysqli_num_rows($result) > 0){
        $data = mysqli_fetch_assoc($result);
    }
    mysqli_close($conn);
    return $data;
  }

  # Updates are in associtive array - fields and the updated data
  # ['password'=>'someUniquePass', 'email'=>'someEmail@some.com']
  # target - 2 item array - ['target field name', 'target field value']
  # ['email','someEmail@something.com']
  function db_updateInfo($table,$updates,$target){
    //Do nothing if attempting to update the userId
    if(count($updates) == 1){
      foreach($updates as $key => $value){
        if($key == 'userId'){
          return;
        }
      }
    }

    $conn = mysqli_connect(DB_HOST,DB_USER,DB_PSWD,DB_NAME);
    if(!$conn){
      echo "Update Data: Connection Failed".mysqli_connect_error();
    }


    #Construct Query
    $query = "UPDATE $table SET";
    foreach($updates as $key => $value){
        //should never update id
        if($key != 'userId'){
          $key     = mysqli_real_escape_string($conn,$key);
          $value   = mysqli_real_escape_string($conn,$value);
          $query  .= ($key == 'active')?" $key = $value,":" $key = '$value',";
        }
    }
    $query = substr($query,0,-1)." WHERE $target[0] = '$target[1]'";
    $result = mysqli_query($conn,$query);
    if(!$result){
        echo('Update Info Failed: '.mysqli_error($conn));
    }

    mysqli_close($conn);
  }

  //Gets the user's directory path
  //Currently just 'userData/[userId]'
  function getUserPath($userEmail){
    $conn = mysqli_connect(DB_HOST,DB_USER,DB_PSWD,DB_NAME);
    if(!$conn){
      echo "Get User Path: Connection Failed".PHP_EOL;
    }
    $query  = "SELECT path FROM Accounts where email='$userEmail'";
    $result = mysqli_query($conn,$query);
    if($result){
      return mysqli_fetch_assoc($result)['path'];
    }
    else{
      echo ("Error description: " . mysqli_error($conn));
    }
  }



?>

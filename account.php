<?php
  //Main Account Page

  //Check if user is logged in and has permission to access this file
  session_start();
  if(!isset($_SESSION['logged_in'])){
    session_unset();
    $_SESSION['logged_in'] = false;
  }
  if(!$_SESSION['logged_in']){
    header('Location: index.html');
    die("Permission Denied: Not Logged In");
  }
  else{
    include "serverFiles/databaseFunctions.php";

    //Get information about the user account to display within the profile section
    $userInfo = db_getInfo('Accounts',['industry','affiliation','position','field'],['email',$_SESSION['email']]);

    //Get user's uploaded data and algorithms
    $algs = array();
    $data = array();

    $inner = getUserPath($_SESSION['email']);

    //user data is temporarily stored within the server files directory
    $path  = 'serverFiles/'.$inner;
    $typeAlg  = '/Algorithms';
    $typeData = '/Datasets';

    if(file_exists($path.$typeAlg)){
      //load all algorithm files within their directory and store their filenames
      foreach(glob($path.$typeAlg.'/*.*') as $filePath){
      	$fileName = explode('/',$filePath);
      	$fileName = $fileName[count($fileName)-1];
        $algs[$fileName] = $inner.$typeAlg.'/'.$fileName;
      }
    }
    if(file_exists($path.$typeData)){
      //load all data files within their directory and store their filenames
      foreach(glob($path.$typeData.'/*.*') as $filePath){
        $fileName = explode('/',$filePath);
        $fileName = $fileName[count($fileName)-1];
        $data[$fileName] = $inner.$typeData.'/'.$fileName;
      }
    }
  }
?><!DOCTYPE html>
<html lan="en-US" dir="ltr">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Account</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- jQuery/Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Font Awesome Gallery - https://fontawesome.com/icons?d=gallery <i class='fas fa-<item>'></i> -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!-- Other Files -->
    <link rel='stylesheet' href='styles/common_css.css'>
    <link rel='stylesheet' href='styles/form_css.css'>
    <link rel='stylesheet' href='styles/account_css.css'>

    <script src='scripts/common_js.js'></script>
    <script src='scripts/form_js.js'></script>
    <script src='scripts/account_js.js'></script>

    </script>
  </head>

  <body onload='checkUserID()'>
    <!--
      Location for any error messages that should pop up.
      Messages are placed within the 'error-msgs' div through javascript and php.
      Same structure as the index.html version.
    -->
    <div id='error-loc' hidden class="fixed-top alert alert-custom alert-dismissible fade show" role='alert'>
      <div id='error-msgs'></div>
      <button type="button" class="close" onclick="$('#error-loc').attr('hidden',true)">
        <i class='fas fa-times-circle' aria-hidden='true'></i>
      </button>
    </div>

    <!-- Use of wrapper around main body and footer to lower the footer all the way to the bottom -->
    <div  id='fill-window'>

      <div class="w-100 dark-theme">
        <div class="container">
          <nav class="navbar navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="https://sites.google.com/a/mail.fresnostate.edu/mri_cloud_sensor/">

              <div class="d-table" id='logoLeft'>
                <div class='d-table-cell align-middle' id='ll-content'>
                  <div>
                    NSF
                  </div>
                  <div>
                    MRI
                  </div>
                </div>
              </div>

              <div class="d-table" id='logoRight'>
                <div class="d-table-cell align-middle">
                  <div>Cloud Based</div>
                  <div class='float-left'>Body Sensor Systems</div>
                </div>
              </div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBar" aria-controls="navBar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navBar">
              <div class="navbar-nav mr-auto">
                <a class="nav-item nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="reference.html">References</a>
                <a class="nav-item nav-link disabled active" id='userDep-Account' href="#">Account <i class="fas fa-lock"></i><span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link disabled" id='userDep-Tool'    href="#">Data & Alg Repository <i class="fas fa-lock"></i></a>
              </div>
              <div class="navbar-nav">
                <a class='nav-item nav-link' id='userDep' href='login.php'>Login</a>
              </div>
            </div>

          </nav>
        </div>
      </div>
      <!-- Push footer all the way to the bottom of the filled window -->
      <main class='mt-4' id='push-footer'>
        <div class="container">
          <div class="row">
            <!-- Side navigation for the account page -->
            <div class="col-auto">
              <div class="list-group" role='tablist' id='sideNav'>
                <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center active" id='list-profile' data-toggle='list' href='#profile-content' role='tab' aria-controls="Profile">
                  Profile
                </a>
                <a class="list-group-item d-flex justify-content-between align-items-center"  id='list-uploads' data-toggle='list' href='#uploads-content' role='tab' aria-controls="Uploads">
                  Uploads
                  <span class="badge badge-dark badge-pill"><?php echo (count($algs)+count($data)); ?></span>
                </a>
                <a class="list-group-item d-flex justify-content-between align-items-center" id='list-approved' data-toggle='list' href='#approved-content' role='tab' aria-controls="Approved">
                  Approved &nbsp;&nbsp;
                  <!-- COMBAK: Unknown Implementation -->
                  <span class="badge badge-dark badge-pill">0</span>
                </a>
                <a class="list-group-item d-flex justify-content-between align-items-center" id='list-settings' data-toggle='list' href='#settings-content' role='tab' aria-controls="Settings">
                  Settings
                </a>
              </div>
            </div>
            <!-- Right Body Content -->
            <div class="col">
              <div class="tab-content">

                <!-- Profile body tab content -->
                <div id='profile-content' class="tab-pane fade show active" role='tabpanel' aria-labelledby="list-profile">
                  <h4 class='title'>Profile</h4>
                  <form class="" action='serverFiles/requestHandler.php' method="POST" id='updateAccount-form'>

                    <!-- Command included here as well. {'cmd':'updateAccount'} -->
                    <input class='d-none' type="text" name="cmd" value="updateAccount">
                    <div class="form-group row">
                      <div class="col">
                        <lable for='firstName'>First Name</label>

                        <div class="input-group">
                          <!-- Input elements should have the same id and name as the database fields that they correspond to -->
                          <input value="<?php echo $_SESSION['fName']?>" id="firstName" name="firstName"  disabled='true' type="text" class="form-control toggle-disable-prop" >

                          <div class="input-group-append">
                            <div class="input-group-text">
                              <!--
                                Checkboxes used to let the user decide what information they wish to update.
                                Name of checkboxes must be the same: updateValues[], or at least by consistent with what is checked at the server.
                                Their id's must begin with 'check-' and end with the name of the element that they correspond too.
                                This helps the serverFiles by just exploding the string at the '-' and having access to the name of the element
                              -->
                              <input name='updateValues[]' id='check-firstName' type="checkbox" aria-label="checkbox button for following text input" disabled='true' class='toggle-disable-prop'>
                            </div>
                          </div>
                        </div>

                        <div id="firstName-error" class='error-tip'></div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col">
                        <lable for='lastName'>Last Name</label>

                        <div class="input-group">
                          <input value="<?php echo $_SESSION['lName']?>" id="lastName"  name="lastName"  disabled='true' type="text" class="form-control toggle-disable-prop" >

                          <div class="input-group-append">
                            <div class="input-group-text">
                              <input name='updateValues[]' id='check-lastName' type="checkbox" aria-label="checkbox button for following text input" disabled='true' class='toggle-disable-prop'>
                            </div>
                          </div>
                        </div>

                        <div id="lastName-error" class='error-tip'></div>
                      </div>
                    </div>



                    <div class="form-group row">
                      <div class="col">
                        <lable for='industry'>Industry</label>

                        <div class="input-group">
                          <input value="<?php echo $userInfo['industry']?>" id="industry"  name="industry"  disabled='true' type="text" class="form-control toggle-disable-prop" >

                          <div class="input-group-append">
                            <div class="input-group-text">
                              <input name='updateValues[]' id='check-industry' type="checkbox" aria-label="checkbox button for following text input" disabled='true' class='toggle-disable-prop'>
                            </div>
                          </div>
                        </div>

                        <div id="industry-error" class='error-tip'></div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col">
                        <lable for='affiliation'>Affiliation</label>

                        <div class="input-group">
                          <input value="<?php echo $userInfo['affiliation']?>" id="affiliation"  name="affiliation" disabled='true' type="text" class="form-control toggle-disable-prop" >

                          <div class="input-group-append">
                            <div class="input-group-text">
                              <input name='updateValues[]' id='check-affiliation' type="checkbox"  disabled='true' class='toggle-disable-prop'>
                            </div>
                          </div>
                        </div>

                        <div id="affiliation-error" class='error-tip'></div>
                      </div>
                    </div>



                    <div class="form-group row">
                      <div class='col'>
                        <label class="form-label" for="pos">Position:</label>

                        <!-- Dropdown list -->
                        <div class='input-group'>
                          <select  disabled='true' id="pos" class="form-control toggle-disable-prop" name="pos">
                            <option value="<?php echo $userInfo['position'];?>" selected><?php echo $userInfo['position'];?></option>
                            <option value='Professor'>Professor</option>
                            <option value='Researcher'>Researcher</option>
                            <option value='Student'>Student</option>
                          </select>
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <input name='updateValues[]' id='check-pos' type="checkbox" disabled='true' class='toggle-disable-prop'>
                            </div>
                          </div>
                        </div>

                        <div id="pos-error" class='error-tip'></div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class='col'>
                        <label class="form-label" for="field">Field:</label>

                        <!-- Dropdown list -->
                        <div class='input-group'>
                          <select  disabled='true'  id="field" class="form-control toggle-disable-prop" name="field">
                            <option value="<?php echo $userInfo['field'];?>" selected><?php echo $userInfo['field'];?></option>
                            <option value='Computer Science'>Computer Science</option>
                            <option value='Biology'>Biology</option>
                            <option value='Medical'>Medical</option>
                            <option value='Chemistry'>Chemistry</option>
                          </select>
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <input name='updateValues[]' id='check-field' type="checkbox" disabled='true' class='toggle-disable-prop'>
                            </div>
                          </div>
                        </div>

                        <div id="field-error" class='error-tip'></div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col">
                        <lable for='email'>Email</label>

                        <div class="input-group">
                          <input value="<?php echo $_SESSION['email']?>" id="email"   name="email" type="text" class="form-control toggle-disable-prop" disabled='true'>

                          <div class="input-group-append">
                            <div class="input-group-text">
                              <input name='updateValues[]' id='check-email' type="checkbox" disabled='true' class='toggle-disable-prop'>
                            </div>
                          </div>
                        </div>

                        <div id="email-error" class='error-tip'></div>
                      </div>
                    </div>
                    <div class="row my-3">
                      <!-- Form Options -->
                      <div class="col">
                        <!-- Edit button toggles disabled property to make inputs editable -->
                        <button type="button" class="btn btn-outline-dark mr-1" onclick="editBtnToggle(this,'toggle-disable-prop');togDisabled('toggle-disable-prop');">Edit</button>
                        <!-- Update button assigns the checkboxes the values of the inputs that they correspond too and submits the form -->
                        <button type="button" class="btn btn-outline-dark ml-1" onclick="assignCheckVals('updateAccount-form');">Update</button>
                      </div>

                    </div>
                  </form>
                </div>


                <!-- Upload tab content -->
                <div id='uploads-content' class="tab-pane fade" role='tabpanel' aria-labelledby="list-uploads">
                  <h4 class='title'>Uploaded Algorithms:</h4>
                  <div class="table-responsive my-2">
                    <table class='table table-striped'>
                      <thead>
                        <tr>
                          <th>File Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <!-- Just a placeholder to see how elements would look like -->
                        <tr>
                          <td>
                            Algo_1
                            <button class='btn btn-outline-dark disabled float-right mx-2' type="button">
                              <i class='fas fa-trash'></i>
                            </button>
                            <button class='btn btn-outline-dark disabled float-right' type="button">
                              <i class='fas fa-cloud-download-alt'></i>
                            </button>
                          </td>
                        </tr>
                        <!-- placholder ends -->

                        <!-- Using php, Algorithm files owned by the user are inserted dynamically -->
                        <?php foreach($algs as $fileName=>$path): ?>
                        <tr>
                          <td>
                            <div class='row'>
                              <div class='col'>
                                <?php echo $fileName; ?>
                              </div>
                              <form class='col-auto' action='serverFiles/downloadFile.php' method='GET'>
                                <!-- Extra info sent over server. {'filePath':value} -->
            		        <input type='text' name='filePath' value="'<?php echo $path; ?>'" class='d-none'>
                                <!-- Delete File option  -->
                                <button onclick="removeFile('<?php echo $path; ?>')" class='btn btn-outline-dark float-right mx-2' type="button">
                                  <i class='fas fa-trash'></i>
                                </button>
                                <!-- Download File option -->
                                <button class='btn btn-outline-dark float-right' type="submit">
                                  <i class='fas fa-cloud-download-alt'></i>
                                </button>
                              </form>
                            </div>
                          </td>
                        </tr>
                        <?php endforeach; ?>

                      </tbody>
                    </table>
                  </div>
                  <h4 class='title'>Uploaded Data:</h4>
                  <div class="table-responsive my-2">
                    <table class='table table-striped'>
                      <thead>
                        <tr>
                          <th>File Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <!-- Data Placeholder demo as well -->
  		                  <tr>
                    			<td>
                  			    Data_1
                  		      <button class='btn btn-outline-dark disabled float-right mx-2' type='button'>
                  			      <i class='fas fa-trash'></i>
                  			    </button>
                  			    <button class='btn btn-outline-dark disabled float-right' type='button'>
                  			      <i class='fas fa-cloud-download-alt'></i>
                  			    </button>
                  			  </td>
                  		  </tr>
                        <!-- Dynamic insertion of data files. Same as Algorithm version -->
                        <?php foreach($data as $fileName=>$path): ?>
                        <tr>
                          <td>
                            <div class='row'>
                              <div class='col'>
                                <?php echo $fileName; ?>
                              </div>
                              <form class='col-auto' action='serverFiles/downloadFile.php' method='GET'>
                                <input type='text' name='filePath' value="'<?php echo $path; ?>'" class='d-none'>
                                <button onclick="removeFile('<?php echo $path; ?>')" class='btn btn-outline-dark float-right mx-2' type="button">
                                  <i class='fas fa-trash'></i>
                                </button>
                                <button class='btn btn-outline-dark float-right' type="submit">
                                  <i class='fas fa-cloud-download-alt'></i>
                                </button>
                              </form>
                            </div>
                          </td>
                        </tr>
                        <?php endforeach; ?>

                      </tbody>
                    </table>
                  </div>

                  <!-- Section that allows users to continue uploading data -->
                  <h4 class='title border-dark my-0'>Upload</h4>
                  <!-- Alg -->
                  <div class="row">
                    <form class='col-sm-12 col-md-6 my-2' action="serverFiles/requestHandler.php" method="POST" enctype="multipart/form-data" id='fileUploadForm-Alg'>
                      <h5>Algorithm</h5>
                      <!--
                        hidden command.
                        hidden extra info. {'fileType':number}. As of now, 0 = algorithm. 1 = data.
                        hidden input file type triggered with fancier looking buttons.
                      -->
                      <input class='d-none' type='text' name='cmd'      value='uploadFile'>
                      <input class='d-none' type='text' name='fileType' value='0'>
                      <input class='d-none' type="file" name="uploadFile-Alg"  id='uploadFile-Alg' >

                      <div class="input-group">

                        <div class="input-group-prepend col px-0">
                          <!-- label updated as file is chosen -->
                          <div class="input-group-text w-100"  id='alg-label'>
                            Choose file
                          </div>
                        </div>

                        <div class="input-group-append col-auto px-0">
                          <!-- button that triggers input file type -->
                          <input class='btn btn-outline-dark' type="button" value='Select File' id="uploadFileLauncher-Alg">
                        </div>
                      </div>

                      <div class="row mt-md-2">
                        <div class="col">
                          <!-- button that triggers submission of the file chosen -->
                          <input class='btn btn-outline-dark' type="button" value="Upload" id='submitBtn-Alg'>
                        </div>
                      </div>
                    </form>

                    <!-- Data -->
                    <!-- Similar to algorithm process -->
                    <form class='col-sm-12 col-md-6 my-2' action="serverFiles/requestHandler.php" method="POST" enctype="multipart/form-data" id='fileUploadForm-Data'>
                      <h5>Data</h5>
                      <input class='d-none' type='text' name='cmd'      value='uploadFile' >
                      <input class='d-none' type='text' name='fileType' value='1'>
                      <input class='d-none' type="file" name="uploadFile-Data"  id='uploadFile-Data'>

                      <div class="input-group">

                        <div class="input-group-prepend col px-0">
                          <!-- FileName label -->
                          <div class="input-group-text w-100" id=data-label>
                            Choose file
                          </div>
                        </div>
                        <div class="input-group-append col-auto px-0">
                          <!-- Trigger file input  -->
                          <input class='btn btn-outline-dark' type="button" value='Select File' id="uploadFileLauncher-Data">
                        </div>
                      </div>
                      <div class="row mt-md-2">
                        <div class="col">
                          <!-- Submit File -->
                          <input class='btn btn-outline-dark' type="button" value="Upload" id='submitBtn-Data'>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>


                <!-- Approved Content Body Tab -->
                <!-- TODO: unkown implementation -->
                <div id='approved-content' class="tab-pane fade" role='tabpanel' aria-labelledby="list-approved">
                  <h4 class='title'>Approved Algorithms:</h4>
                  <div class="table-responsive my-2">
                    <table class='table table-striped'>
                      <thead>
                        <tr>
                          <th>File Name</th>
                        </tr>
                      </thead>
                      <tbody id='approved-algs-loc'></tbody>
                    </table>
                  </div>
                </div>

                <!-- Settings Content Body Tab -->
                <div id='settings-content' class="tab-pane fade" role='tabpanel' aria-labelledby="list-settings">
                  <div class="list-group">
                    <!-- Change Password Option -->
                    <button id='changePswd-tab' type='button' href="#changePswd" data-toggle="collapse" class='list-group-item list-group-item-action' role="button" aria-expanded="false" aria-controls="changePswd">Update Password</button>
                      <div id="changePswd" class='tab-pane collapse'  role='tabpanel' aria-labelledby="changePswd-tab">
                        <form class="" id='pswdUpdate-form' name='pswdUpdate-form' accept-charset="UTF-8" action="" method="" autocomplete='off'>
                          <!-- Hidden Command. {'cmd':'updatePswd'} -->
                          <input class='d-none' type="text" name="cmd" value="updatePswd">
                          <dl class="form-group">
                            <dt class="input-label">
                              <label class="form-label" for="og-pswd">Password:</label>
                            </dt>
                            <dd>
                              <input onkeyup="listenForEnter('#submit-update',event);" type="password" name="og-pswd" id="og-pswd" class="form-control form-control-lg" placeholder="********" autocomplete='off' required>
                            </dd>
                          </dl>
                          <dl class="form-group">
                            <dt class="input-label">
                              <label class="form-label" for="new-pswd">New Password:</label>
                            </dt>
                            <dd>
                              <!--
                                applyColor applies green or red borders depending on whether its a valid password:
                                    for now, just at least one character with no spaces
                                pswdCheck checks given id's values to see if they match
                              -->
                              <input onkeyup="listenForEnter('#submit-update',event);applyColor(this,pswdCheck('new-pswd','new-pswd-check'));applyColor('#new-pswd-check',pswdCheck('new-pswd','new-pswd-check'));" type="password" name="new-pswd" id="new-pswd" class="form-control form-control-lg" placeholder="********" autocomplete='off' required>
                            </dd>
                          </dl>
                          <dl class="form-group">
                            <dt class="input-label">
                              <label class="form-label" for="new-pswd-check">Re-Type New Password:</label>
                            </dt>
                            <dd>
                              <input onkeyup="listenForEnter('#submit-update',event);applyColor(this,pswdCheck('new-pswd','new-pswd-check'));applyColor('#new-pswd',pswdCheck('new-pswd','new-pswd-check'));" type="password" name="new-pswd-check" id="new-pswd-check" class="form-control form-control-lg" placeholder="********" autocomplete='off' required>
                            </dd>
                          </dl>
                          <button class="btn btn-outline-dark " type="button" id='submit-update' onclick="updatePswd('pswdUpdate-form')">Update Password</button>
                        </form>
                      </div>

                    <!-- COMBAK: unknown if Implementation required -->
                    <!-- Delete Account and all its data -->
                    <button id='deleteAccount-tab' type='button' href="#deleteAccount" data-toggle="collapse" class='list-group-item list-group-item-action' role="button" aria-expanded="false" aria-controls="deleteAccount">Delete Account</button>
                      <div id="deleteAccount" class='tab-pane collapse' role='tabpanel' aria-labelledby="deleteAccount-tab">
                        <div class="my-2">
                          <button type='button' class='btn btn-outline-danger' role="button" onclick="">Delete Account</button>
                          <button type='button' class='btn btn-outline-dark' role="button" onclick="">Cancel</button>
                        </div>
                      </div>

                    <!-- COMBAK: unknown if Implementation required -->
                    <!-- Delete data such as all algorithm and data files -->
                    <button id='deleteData-tab' type='button' href="#deleteData" data-toggle="collapse" class='list-group-item list-group-item-action' role="button" aria-expanded="false" aria-controls="deleteData">Erase Data</button>
                      <div id="deleteData" class='tab-pane collapse' role='tabpanel' aria-labelledby="deleteData-tab">
                        <div class="my-2">
                          <button type='button' class='btn btn-outline-danger' role="button" onclick="">Erase Data</button>
                          <button type='button' class='btn btn-outline-dark' role="button" onclick="">Cancel</button>
                        </div>
                      </div>
                      <button type='button' class='list-group-item list-group-item-action' role="button" onclick="signOut()">Sign Out</button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      <footer id='theFooter'>
        <!-- Sponsor logos if required -->
        <!-- <div class="container py-3" id='footerUpper'>
          <div class="row">
            <div class="col-12" id='sponsorImgs'>
              <div class="row">
                <div class="col-5 align-self-center ">
                  <div class="logoBox">
                    <img id='fsLogo' src="imgs/Fresno_State_Logo.png" alt="College Logo"/>
                  </div>
                </div>
                <div class="col-2 align-self-center ">
                  <div class="logoBox">
                    <img class='' id='nsfLogo' src="imgs/nsflogo.png" alt='NSF Logo'/>
                  </div>
                </div>
                <div class="col-5 align-self-center ">
                  <div class="logoBox">
                    <img id='utLogo' src="imgs/ut-dallas-logo.jpg" alt="UT Dallas Logo"/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <div class="jumbotron" id="footerLinksWrap">
          <div class="container py-1">
            <div class="row align-items-center mb-2">

              <div class="col">
                <div class="row" id='footerLinks'>
                  <div class="col">
                    <a href="http://www.csufresno.edu/">Fresno State</a>
                  </div>
                  <div class="col">
                    <a href="https://www.nsf.gov/">National Science Foundation</a>
                  </div>
                  <div class="col">
                    <a href="https://www.utdallas.edu/">UT Dallas</a>
                  </div>
                  <div class="col">
                    <a href="https://sites.google.com/a/mail.fresnostate.edu/mri_cloud_sensor/">Project Page</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mt-2">
              <div class="col">
                Copyright &copy; 2016 - 2018 National Science Foundation
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>
</html>

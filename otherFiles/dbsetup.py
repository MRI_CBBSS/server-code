#!/usr/bin/python
#Script to configure the initial setup of the database
import MySQLdb as mysqldb

config = {
    'host': 'localhost',
    'user': '*****',
    'pass': '***********',
    'dbName':'csufml_db'
}

"""
try:
    db  = mysqldb.connect(config['host'],config['user'],config['pass'])
except mysqldb.Error:
    raise mysqldb.error
except mysqldb.Warning:
    pass
cursor = db.cursor()
query  = "CREATE DATABASE " + config['dbName']
#data = cursor.fetchone() to get one row. fetchall() for all
try:
    cursor.execute(query)
    #db.comit() #to save changes to db
except (mysqldb.OperationalError,mysqldb.ProgrammingError),e:
    raise e
    #db.rollback() #rollback if any errors occur

print "done"
db.close()

#('val("%s","%d")'%\("hi",3))
"""


try:
    db = mysqldb.connect( config['host'], config['user'], config['pass'])
except mysqldb.Error, e:
    print(e)
else:
    cursor = db.cursor()

query = "CREATE DATABASE " + config['dbName']

try:
    cursor.execute(query)
    #db.comit()
except mysqldb.Error, e:
    if (e[0] == 1007):
        print "Database exists already."
        print "Switching to use it."
        cursor.execute("USE csufml_db");
    else: print(e)
else:
    cursor.execute("USE csufml_db");

#user - id, email, path
tables = {}

#algorithm - id, name, filename, path, format
tables['Algorithms'] = """CREATE TABLE Algorithms
(
        algId           INT unsigned NOT NULL AUTO_INCREMENT,
        userId          INT UNSIGNED NOT NULL,
        resourceType    VARCHAR(40) NOT NULL,
        fileName        VARCHAR(60) NOT NULL,
        format          SET('nominal', 'ordinal', 'interval', 'ratio'),
        PRIMARY KEY(algId)
);"""

#data - id, name, path, format
tables['Datasets'] = """CREATE TABLE Datasets
(
        dataId          INT UNSIGNED NOT NULL AUTO_INCREMENT,
        userId          INT UNSIGNED NOT NULL,
        resourceType    VARCHAR(40) NOT NULL,
        fileName        VARCHAR(60) NOT NULL,
        path            VARCHAR(512),
        format          SET('nominal', 'ordinal', 'interval', 'ratio'),
        PRIMARY	KEY(dataId)
);"""

#resource - In case any special info is needed
tables['ResourceInfo'] = """CREATE TABLE ResourceInfo
(
        resourceType    VARCHAR(40),
        path            VARCHAR(512)
);"""

#simulations - userid, algorithm, dataset, timeStart, timeEnd
tables['Simulations'] = """CREATE TABLE Simulations
(
        simId       INT UNSIGNED NOT NULL AUTO_INCREMENT,
        userId      INT unsigned NOT NULL,
        algorithm   VARCHAR(60),
        dataset	    VARCHAR(60),
        tstart      TIMESTAMP,
        tend        TIMESTAMP,
        PRIMARY KEY(simId)
);"""


#temp design for account information of a user
tables['Accounts'] = """CREATE TABLE Accounts
(
        userId      INT UNSIGNED NOT NULL AUTO_INCREMENT,
        firstName   VARCHAR(20) NOT NULL,
        lastName    VARCHAR(30) NOT NULL,
        industry    VARCHAR(30) NOT NULL,
        affiliation VARCHAR(30) NOT NULL,
        position    VARCHAR(30) NOT NULL,
	field       VARCHAR(30) NOT NULL,
        email       VARCHAR(100) NOT NULL,
        password    VARCHAR(100) NOT NULL,
        path        VARCHAR(512),

        hash        VARCHAR(32) NOT NULL DEFAULT 0,
        active      BOOL NOT NULL DEFAULT 0,

        PRIMARY KEY(userId)
);"""
for key in tables.keys():
    try:
        print "Creating table:", key
        cursor.execute(tables[key])
    except mysqldb.Error, e:
    	if (e[0] == 1050):
        	print "Table exists, continuing"
        	continue
    	else:
    		print (e)
print "Tables Finished"
userLuis = """INSERT INTO Accounts
(
    firstName,lastName,industry,affiliation,position,
    email,password,path,field,hash,active
)
VALUES
(
    'Luis','Salazar','Software','Fresno State','student',
    'luis2967@mail.fresnostate.edu','$2y$10$TQzj3Mb3sMQwTgsihrrAke.PM1HKpNZ6ti009rBpZpQfiSIoP9DUa',
    'userData/1','computer science' ,'d1f255a373a3cef72e03aa9d980c7eca',0
)
"""
try:
    cursor.execute(userLuis)
except mysqldb.Error,e:
    print e
db.commit()
db.close()
print "done"
